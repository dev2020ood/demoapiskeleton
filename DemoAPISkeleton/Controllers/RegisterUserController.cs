﻿using DemoAPISkeletonContracts.DTO;
using DemoAPISkeletonContracts.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoAPISkeleton.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterUserController : ControllerBase
    {
        IRegisterService _registerService;

        public RegisterUserController(IRegisterService registerService)
        {
            _registerService = registerService;
        }
        // GET: api/<RegisterUserController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<RegisterUserController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<RegisterUserController>
        [HttpPost]
        public RegisterResponse Post([FromBody] RegisterRequest request)
        {
            return _registerService.Register(request);
        }

        // PUT api/<RegisterUserController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<RegisterUserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
