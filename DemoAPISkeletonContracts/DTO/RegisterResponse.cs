﻿using Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoAPISkeletonContracts.DTO
{
    abstract public class RegisterResponse:Response
    {

    }
    public class RegisterResponseOK: RegisterResponse
    {

    }
    public class UserExitsResponse: RegisterResponse
    {

    }
    public class RegisterResponseError : ResponseError 
    {
    }
}
