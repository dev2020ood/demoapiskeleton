﻿using DemoAPISkeletonContracts.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoAPISkeletonContracts.Interfaces
{
    public interface IRegisterService
    {
        RegisterResponse Register(RegisterRequest request); 

    }
}
