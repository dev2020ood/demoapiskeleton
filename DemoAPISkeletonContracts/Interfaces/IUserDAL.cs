﻿using DemoAPISkeletonContracts.DTO;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DemoAPISkeletonContracts.Interfaces
{
    public interface IUserDAL
    {

       
        public DataSet getUser(string userID);
        public DataSet insertUser(UserDTO user);
    }
}
