﻿using Microsoft.AspNetCore.Mvc;
using System;


namespace MyExternalController
{
    [Route("api/[controller]")]
    [ApiController]
    public class TheController:ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "Hello";
        }

    }
}
