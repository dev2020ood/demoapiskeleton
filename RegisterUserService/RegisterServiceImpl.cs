﻿using Contracts;
using DemoAPISkeletonContracts.DTO;
using DemoAPISkeletonContracts.Interfaces;
using System;

namespace RegisterUserService
{
    [Register(Policy.Transient,typeof(IRegisterService))]
    public class RegisterServiceImpl : IRegisterService
    {
        IUserDAL _dal;
        public RegisterServiceImpl(IUserDAL dal)
        {
            _dal = dal; 
        }
        public RegisterResponse Register(RegisterRequest request)
        {
            RegisterResponse retval = new UserExitsResponse(); 
            var userDS = _dal.getUser(request.user.EMail);

            if (userDS.Tables[0].Rows.Count == 0)
            {
                var ds = _dal.insertUser(request.user);
                //Check DS 
                retval = new RegisterResponseOK();

            }
            return retval; 
        }
    }
}
