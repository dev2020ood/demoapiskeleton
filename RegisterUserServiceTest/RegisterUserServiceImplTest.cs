using DemoAPISkeletonContracts.DTO;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using RegisterUserService;
using System;
using System.Collections.Generic;
using UserDAL;

namespace RegisterUserServiceTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

            //Create stub for both user exists and regular userr 
           
        }

        [Test]
        public void UserExistsTest()
        {
            var registerService = new RegisterServiceImpl(new UserDALUserExistsStub());
            var request = new RegisterRequest();
            request.user = new UserDTO();
            request.user.EMail = "AAA";
            request.user.UserName = "AAA";
            request.user.Password= "AAA";
            var response = registerService.Register(request);
            Assert.IsInstanceOf(typeof(UserExitsResponse), response);




        }
        [Test]
        public void RegisterOKTest()
        {

        }
        [Test]
        public void RegisterRaiseException()
        {
            Assert.Throws<Exception>(() => new UserDALWithException());
            

        }
    }
}