﻿using DemoAPISkeletonContracts.DTO;
using DemoAPISkeletonContracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace RegisterUserServiceTest
{
    class UserDALUserExistsStub : IUserDAL
    {
        public DataSet getUser(string userID)
        {
            var dt = new DataTable();
            var retval = new DataSet();
            retval.Tables.Add(dt);
            return new DataSet(); 
        }

        public DataSet insertUser(UserDTO user)
        {
            return null; 
        }
    }
}
