﻿using DalInfraContracts.Interfaces;
using DemoAPISkeletonContracts.DTO;
using DemoAPISkeletonContracts.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;

namespace UserDAL
{
    public class UserDALImpl : IUserDAL
    {
        IConfiguration _configuration;
        IInfraDAL _dal;
        public UserDALImpl(IConfiguration configuration,IInfraDAL dal)
        {
            _configuration = configuration;
            _dal = dal; 
        }
        public DataSet getUser(string userID)
        {
            try
            {
                var connection = _dal.GetConnection(_configuration.GetConnectionString("AppDB"));
                var p_userID = _dal.GetParameter("UserID", userID);
                var retval = _dal.Exec(connection, "GetUser", p_userID);
                return retval;
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }

        public DataSet insertUser(UserDTO user)
        {
            try
            {
                var connection = _dal.GetConnection(_configuration.GetConnectionString("AppDB"));
                var p_userName = _dal.GetParameter("UserName", user.UserName);
                var p_EMail = _dal.GetParameter("EMail", user.EMail);
                var p_Password = _dal.GetParameter("Password", user.Password);
                var retval = _dal.Exec(connection, "InserUser", p_userName,p_EMail,p_Password);
                return retval;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
